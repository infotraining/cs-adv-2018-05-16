﻿using System;

namespace PluginsBase
{
    public class PluginInfoAttribute : Attribute
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
