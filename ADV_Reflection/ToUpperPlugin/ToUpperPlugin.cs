﻿using PluginsBase;
using System;

namespace ToUpperPlugin
{
    [PluginInfo(Name = "To upper")]
    public class ToUpperPlugin : ITextTransform
    {
        public string Tranform(string text)
        {
            return text.ToUpper();
        }
    }
}
