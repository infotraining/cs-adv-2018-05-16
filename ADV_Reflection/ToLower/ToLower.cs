﻿using PluginsBase;

namespace ToLowerPlugin
{
    [PluginInfo(Name = "To lower")]
    public class ToLower : ITextTransform
    {
        public string Tranform(string text)
        {
            return text.ToLower();
        }
    }
}
