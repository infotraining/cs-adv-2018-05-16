﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ReflectingAttributes
{
    class Program
    {
        static void Main(string[] args)
        {
            Assembly assembly = Assembly.Load("CustomAttributes");
            Type[] t = assembly.GetTypes();

            foreach (Type type in t)
            {
                Console.WriteLine(type.FullName);
            }

            Type vehicleDescription = 
                assembly.GetType("CustomAttributes.VehicleDescriptionAttribute");

            PropertyInfo description = vehicleDescription.GetProperty("Description");

            Console.WriteLine("Custom attributes");

            foreach (Type type in t)
            {
                object[] obj = type.GetCustomAttributes(vehicleDescription, false);

                foreach (var o in obj)
                {
                    Console.WriteLine($" -> {type.Name} : {description.GetValue(o)}");
                }
            }
        }
    }
}
