﻿using PluginsBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimplePluginArchitecture
{
    public partial class Form1 : Form
    {
        private int y = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void toolStripDropDownButton1_Click(object sender, EventArgs e)
        {

        }

        private void loadPluginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if (!LoadPlugin(openFileDialog1.FileName))
                {
                    MessageBox.Show("Plik jest niekombatybilny");
                }
            }
        }

        private string GetPluginName(Type type)
        {
            var pluginInfo = (PluginInfoAttribute)
                                (from pi in type.GetCustomAttributes(false)
                                  where pi.GetType() == typeof(PluginInfoAttribute)
                                  select pi).First();

            return pluginInfo.Name;
        }

        private bool LoadPlugin(string fileName)
        {
            bool isPluginFound = false;
            Assembly pluginAssembly = null;

            try
            {
                pluginAssembly = Assembly.LoadFrom(fileName);
                var classTypes = from t in pluginAssembly.GetTypes()
                                 where (t.IsClass && t.GetInterface("ITextTransform") != null)
                                 select t;

                foreach (Type type in classTypes)
                {
                    ITextTransform plugin = 
                        (ITextTransform)Activator.CreateInstance(type, true);

                    var button = new Button { Text = GetPluginName(type), Location = new Point(0, y) };

                    y += 25;

                    button.Click += new EventHandler((sender, e) => click(sender, e, plugin));

                    panel1.Controls.Add(button);

                    isPluginFound = true;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return isPluginFound;
        }

        private void click(object sender, EventArgs e, ITextTransform plugin)
        {
            string text = plugin.Tranform(textBox1.Text);
            textBox1.Text = text;
        }
    }
}
