﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ReflectingDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Type t1 = DateTime.Now.GetType();
            Console.WriteLine(t1.Name);

            Type t2 = typeof(DateTime);
            Console.WriteLine(t2.Name);

            Console.WriteLine(typeof(DateTime[]));
            Console.WriteLine(typeof(Dictionary<int,string>));

            Type t3 = Assembly.GetExecutingAssembly().GetType("ReflectingDemo.Program");

            Console.WriteLine(t3);

            Type t4 = Type.GetType("System.Int32, mscorlib, Version=2.0.0.0, Culture=neutral, " +
                "PublicKeyToken=b77a5c5611934e089");

            Console.WriteLine();

            Type t5 = typeof(System.Text.StringBuilder);
            Console.WriteLine(t5.Namespace);
            Console.WriteLine(t5.Name);
            Console.WriteLine(t5.FullName);

            Type t6 = typeof(System.Environment.SpecialFolder);
            Console.WriteLine(t6.Namespace);
            Console.WriteLine(t6.Name);
            Console.WriteLine(t6.FullName);

            Console.WriteLine();

            Console.WriteLine(typeof(Helper).GetMethod("Check").GetParameters()[0].ParameterType);

            Type t7 = typeof(System.String).BaseType;
            Console.WriteLine(t7);

            Type t8 = typeof(System.IO.FileStream).BaseType;
            Console.WriteLine(t8);

            Console.WriteLine();

            foreach (Type type in typeof(Guid).GetInterfaces())
            {
                Console.WriteLine(type.Name);
            }

            Console.WriteLine();

            object obj = "Hello World";
            Type tar = typeof(IComparable);
            Console.WriteLine(obj is IComparable);
            Console.WriteLine(tar.IsInstanceOfType(obj));

            //IsAsssignableFrom

            Type source = typeof(string);
            Type target = typeof(IComparable);

            Console.WriteLine(target.IsAssignableFrom(source));

            int i = (int)Activator.CreateInstance(typeof(int));

            ConstructorInfo ci = typeof(Helper).GetConstructor(new[] { typeof(string) });

            object test = ci.Invoke(new object[] { null });

            Console.WriteLine();

            Delegate staticDelegate = Delegate.CreateDelegate(typeof(IntFunction), typeof(Program), "Square");
            Delegate instanceDelegate = Delegate.CreateDelegate(typeof(IntFunction), new Program(), "Cube");

            Console.WriteLine(staticDelegate.DynamicInvoke(3));
            Console.WriteLine(instanceDelegate.DynamicInvoke(3));
        }

        delegate int IntFunction(int x);

        static int Square(int x) { return x * x; }
        int Cube(int x) { return x * x * x; }

        class Helper
        {
            private string _message;

            public Helper()
            {

            }

            public Helper(string message)
            {
                _message = message;
            }

            public bool Check(out bool ch)
            {
                ch = false;

                return true;
            }
        }
    }
}
