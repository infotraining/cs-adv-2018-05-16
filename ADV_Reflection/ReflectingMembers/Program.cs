﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ReflectingMembers
{
    class Program
    {
        static void Main(string[] args)
        {
            MemberInfo[] members = typeof(Engine).GetMembers();

            foreach (var member in members)
            {
                Console.WriteLine(member);
            }

            Console.WriteLine();

            IEnumerable<MemberInfo> membersFromTypeInfo = 
                typeof(Engine).GetTypeInfo().DeclaredMembers;

            foreach (var mfti in membersFromTypeInfo)
            {
                Console.WriteLine(mfti);
            }

            Console.WriteLine();


            //dynamiczny dostęp do właściwości
            object text = "Hello";

            PropertyInfo pi = text.GetType().GetProperty("Length");

            int lengthOfText = (int)pi.GetValue(text);

            Console.WriteLine($"Zmienna text ma długość {lengthOfText}");

            //dynamiczny dostęp do metody
            Type type = typeof(string);
            Type[] parametrTypes = { typeof(int) };

            MethodInfo mi = type.GetMethod("Substring", parametrTypes);

            object[] arguments = { 2 };
            object returnValue = mi.Invoke("Reflection", arguments);

            Console.WriteLine("Substring z Reflection: " + returnValue);

            //użycia dynamic
            Type type1 = Assembly.GetExecutingAssembly().GetType("ReflectingMembers.Engine");
            dynamic t = Activator.CreateInstance(type1);
            t.Start();

            //dostęp do składowych niepublicznych
            Type e = typeof(Engine);

            Engine engine = new Engine();
            engine.Start();

            FieldInfo fi = e.GetField("_running", BindingFlags.NonPublic | BindingFlags.Instance);
            fi.SetValue(engine, false);

            Console.WriteLine(engine.IsRunning());
        }
    }

    class Engine
    {
        private bool _running;
        public void Start() { Console.WriteLine("Uruchamianie silnika"); _running = true; }
        public bool IsRunning() { return _running;}
    }
}
