﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomAttributes
{
    [Serializable]
    [VehicleDescription("Luksusowa niemiecka limuzyna")]
    public class Mercedes
    {
    }

    [Serializable]
    [VehicleDescription("Japończyk")]
    public class Mazda
    {

    }

    [Obsolete]
    [VehicleDescription("Szybszy od światła")]
    public class BMW
    {

    }
}
