﻿using System;

namespace CustomAttributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = false, Inherited = false)]
    public class VehicleDescriptionAttribute : Attribute
    {
        private string msgData;

        public VehicleDescriptionAttribute()
        {

        }

        public VehicleDescriptionAttribute(string description)
        {
            msgData = description;
        }

        public string Description
        {
            get { return msgData; }
            set { msgData = value; }
        }
    }
}
