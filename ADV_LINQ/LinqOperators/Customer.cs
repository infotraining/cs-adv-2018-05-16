﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqOperators
{
    class Customer
    {
        public Customer(int _ID, string _name)
        {
            ID = _ID;
            Name = _name;
            Orders = new List<Order>();
        }

        public int ID { get; set; }
        public string Name { get; set; }

        public List<Order> Orders { get; set; }
    }

    class Order
    {
        public Order(int customerID, string product, decimal price)
        {
            CustomerID = customerID;
            Product = product;
            Price = price;
        }

        public int CustomerID { get; set; }
        public string Product { get; set; }
        public decimal Price { get; set; }
    }
}
