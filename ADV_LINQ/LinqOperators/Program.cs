﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqOperators
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] names = { "Łukasz", "Krzysztof", "Rafał" };
            int[] numbers = { 23, 533, 35, 0, 1, 65, 35 };

            var customers = new List<Customer>
            {
                new Customer(1, "Google"),
                new Customer(2, "Microsoft"),
                new Customer(3, "ABB"),
                new Customer(4, "ARiMR")
            };

            customers[0].Orders.Add(new Order(1, "Drukarka XEROX", 1500.0M));
            customers[1].Orders.Add(new Order(2, "Laptop ASUS", 2000.0M));
            customers[1].Orders.Add(new Order(2, "Router LINKSYS", 50.0M));
            customers[2].Orders.Add(new Order(3, "Monitor HP", 150.0M));

            var queryNames = names.Where((n, i) => (n.Contains("ł") || n.Contains("Ł")) && i == 0);

            foreach (var item in queryNames)
            {
                Console.WriteLine(item);
            }

            //TakeWhile, SkipWhile

            var query1 = numbers.TakeWhile(n => n > 10);

            foreach (var item in query1)
            {
                Console.Write(item + " ");
            }

            Console.WriteLine();

            var query2 = numbers.SkipWhile(n => n > 10);

            foreach (var item in query2)
            {
                Console.Write(item + " ");
            }

            Console.WriteLine();

            //Distinct
            var query3 = numbers.Distinct();

            foreach (var item in query3)
            {
                Console.Write(item + " ");
            }

            Console.WriteLine();

            //Indeksowany Select

            var query4 = names.Select((s, i) => i + ": " + s);

            foreach (var item in query4)
            {
                Console.Write(item + " " );
            }
            Console.WriteLine();

            //Subquery

            //var query5 = from c in customers
            //             where c.Orders.Any(p => p.Price > 1000.0M)
            //                select new
            //                {
            //                    c.Name,
            //                    Items = from o in c.Orders
            //                            where o.Price > 1000.0M
            //                            select new
            //                            {
            //                                o.Product,
            //                                o.Price
            //                            }
            //                };

            var query5 = from c in customers
                         let Items = from o in c.Orders
                                         where o.Price > 1000.0M
                                         select new { o.Product, o.Price }
                         where Items.Any()
                         select new { c.Name, Items };

            foreach (var customer in query5)
            {
                Console.WriteLine(customer.Name);
                foreach (var item in customer.Items)
                {
                    Console.WriteLine(" -- " + item.Product + ", " + item.Price);
                }
            }

            //SelectMany

            string[] fullNames = { "Łukasz Zawadzki", "Krzysztof Kowalski", "Rafał Wieleba" };

            //var query6 = fullNames.SelectMany(n => n.Split());

            var query6 = fullNames.Select(n => n.Split());

            foreach (var item in query6)
            {
                foreach (var item2 in item)
                {
                    Console.Write(item2 + " ");
                }

                //Console.Write(item + " ");
            }
            Console.WriteLine();

            var query7 = from f in fullNames
                         from n in f.Split()
                         where n.Length > 6
                         select n + ": " + f;

            foreach (var item in query7)
            {
                Console.WriteLine(item + " ");
            }
            Console.WriteLine();

            var query8 = fullNames.SelectMany(f => f.Split().Select(n => new { n, f })
                        .Where(x => x.n.Length > 6).Select(x => x.n + " jest w " + x.f));

            foreach (var item in query8)
            {
                Console.WriteLine(item + " ");
            }
            Console.WriteLine();

            //SelectMany crossjoin

            var query9 = from n in names
                         from n1 in names
                         where n.CompareTo(n1) < 0
                         select n + " - " + n1;

            foreach (var item in query9)
            {
                Console.WriteLine(item);
            }

            //SelectMany innerjoin
            var query10 = from c in customers
                          from o in c.Orders.DefaultIfEmpty()
                          select new
                          {
                              c.Name,
                              Product = o?.Product
                          };

            foreach (var item in query10)
            {
                Console.WriteLine(item.Name + " kupił " + item.Product);
            }

            //Join i Groupjoin
            Console.WriteLine("Join");
            var query11 = customers.Join(customers.SelectMany(c => c.Orders), 
                                                    c => c.ID, 
                                                    o => o.CustomerID, 
                                                    (c, p) => c.Name + " kupił " + p.Product);

            foreach (var item in query11)
            {
                Console.WriteLine(item);
            }

            var orders = customers.SelectMany(c => c.Orders);

            var query12 = from c in customers
                          join p in orders on c.ID equals p.CustomerID
                          select c.Name + " kupił " + p.Product;

            Console.WriteLine("Query12");
            foreach (var item in query12)
            {
                Console.WriteLine(item);
            }

            //Groupjoin
            Console.WriteLine("GroupJoin");
            var query13 = customers.GroupJoin(customers.SelectMany(c => c.Orders),
                                                    c => c.ID,
                                                    o => o.CustomerID,
                                                    (c, o) => new { c.Name, o });

            foreach (var query in query13)
            {
                Console.WriteLine(query.Name);
                foreach (var item in query.o)
                {
                    Console.WriteLine(" - " + item.Product);
                }
            }

            //Zip
            var query14 = numbers.Zip(names, (n, n1) => n + " = " + n1);

            foreach (var item in query14)
            {
                Console.WriteLine(item);
            }

            //Sortowania
            //OrderBy, ThenBy, Reverse

            var query15 = names.OrderByDescending(n => n.Length).ThenBy(n => n);

            var query16 = from n in names
                          orderby n.Length descending, n
                          select n;

            //Grupowanie
            //GroupBy

            var query17 = customers.SelectMany(c => c.Orders).GroupBy(o => o.CustomerID);

            foreach (var _orders in query17)
            {
                Console.WriteLine("CustomerID: " +  _orders.Key);
                foreach (var order in _orders)
                {
                    Console.WriteLine(" - " + order.Product);
                }
            }

            // operatory zbiorów
            // Concat, Union, Intersect, Except
            int[] num1 = { 1, 1, 2, 2, 3, }, num2 = { 2, 3, 6 };

            foreach (var item in num1.Concat(num2))
            {
                Console.Write(item + " | ");
            }
            Console.WriteLine();
            foreach (var item in num1.Union(num2))
            {
                Console.Write(item + " | ");
            }
            Console.WriteLine();
            foreach (var item in num1.Intersect(num2))
            {
                Console.Write(item + " | ");
            }
            Console.WriteLine();
            foreach (var item in num1.Except(num2))
            {
                Console.Write(item + " | ");
            }
            Console.WriteLine();

            //operatory elementowy
            Console.WriteLine(numbers.FirstOrDefault(n => n > 1000));

            Console.WriteLine(numbers.Single(n => n > 500));

            Console.WriteLine(customers.ElementAt(2).Name);

            //operatory agregacji
            Console.WriteLine(numbers.Count(n => n > 20));

            Console.WriteLine(numbers.Max());
            Console.WriteLine(customers.SelectMany(c => c.Orders).Min(o => o.Price));

            Console.WriteLine(numbers.Sum());
            Console.WriteLine(numbers.Average());

            Console.WriteLine(numbers.Aggregate(0, (total, n) => total + n));

            //kwantyfikatory
            Console.WriteLine(numbers.Contains(3));
            Console.WriteLine(numbers.Where(n => n > 3).Any());

            Console.WriteLine(numbers.All(n => n > 6));

            Console.WriteLine(num1.SequenceEqual(num2));

            //generatory
            foreach(int i in Enumerable.Range(0, 10))
            {
                Console.Write(i + " ");
            }

            foreach (var item in Enumerable.Repeat(2, 10))
            {
                Console.WriteLine(item + " ");
            }

            var ran = new Random();
 
            // create a delegate from the method group,
            // repeat that delegate 100 times,
            // for each delegate, project it to its result,
            // convert to array.
            var results = Enumerable.Repeat<Func<int>>(ran.Next, 100)
                .Select(f => f())
                .ToArray();

            foreach (var item in results)
            {
                Console.WriteLine(item + " ");
            }
        }
    }
}
