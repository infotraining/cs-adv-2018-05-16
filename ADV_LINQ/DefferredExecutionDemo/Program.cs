﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DefferredExecutionDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var numbers = new List<int> { 1, 2, 3, 14 };

            int factor = 10;

            var query = numbers.Where(n => n > factor);

            factor = 20;

            foreach (var item in query)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            ExecuteActions();

            IEnumerable<char> query1 = "Szkolenie z zaawansowanego C#";
            string vowels = "aeyuio";

            //for (int i = 0; i < vowels.Length; i++)
            //{
            //    char temp = vowels[i];
            //    query1 = query1.Where(s => s != temp);
            //}

            foreach (char item in vowels)
            {
                query1 = query1.Where(c => c != item);
            }

            foreach (var item in query1)
            {
                Console.Write(item);
            }

            Console.WriteLine();

            //Action a = () => Console.WriteLine("Z delegata...");

            //var query = numbers.Where(n => n > 2);

            //numbers.Add(4);

            //foreach (var item in query)
            //{
            //    Console.WriteLine(item);
            //}

            //a();
        }

        static void ExecuteActions()
        {
            Action[] actions = new Action[3];

            for (int i = 0; i < actions.Length; i++)
            {
                int temp = i;
                actions[temp] = () => Console.WriteLine("Iteracja " + temp);
            }

            foreach (var a in actions)
            {
                a();
            }
        }
    }
}
