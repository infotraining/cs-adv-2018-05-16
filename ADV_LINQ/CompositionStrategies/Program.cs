﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CompositionStrategies
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] names = { "Łukasz Zawadzki", "Krzysztof Kowalski", "Rafał Wieleba" };

            var query = names.Select(n => Regex.Replace(n, "[aeuioy]", ""))
                                .Where(n => n.Length > 9)
                                .OrderBy(n => n);

            foreach (var item in query)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            //var querySyntax = from n in names
            //                  select Regex.Replace(n, "[aeuioy]", "");

            //querySyntax = from n in querySyntax
            //              where n.Length > 9
            //              orderby n
            //              select n;                

            ////into
            //var querySyntax = from n in names
            //                  select Regex.Replace(n, "[aeuioy]", "")
            //                  into noVowels
            //                  where noVowels.Length > 9
            //                  orderby noVowels
            //                  select noVowels;

            //wrapping query
            var querySyntax = from n in (
                from n2 in names
                select Regex.Replace(n2, "[aeuioy]", "")
            )
                              where n.Length > 9
                              orderby n
                              select n;


            foreach (var item in querySyntax)
            {
                Console.WriteLine(item);
            }
        }
    }
}
