﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Subqueries
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] names = { "Łukasz Zawadzki", "Krzysztof Kowalski", "Rafał Wieleba" };

            IEnumerable<string> query = names.Select(n => n.Split().Last());

            IEnumerable<string> query1 = query.Where
                (n => n.Length == query.OrderBy(n2 => n2.Length).Select(n2 => n2.Length).First());

            foreach (var item in query1)
            {
                Console.WriteLine(item);
            }
        }
    }
}
