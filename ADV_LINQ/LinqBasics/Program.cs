﻿using System;
using System.Linq;

namespace LinqBasics
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] names = { "Łukasz", "Krzysztof", "Rafał" };

            //IEnumerable<string> filteredNames = System.Linq.Enumerable
                                                        //.Where(names, n => n.Length >= 6);

            int filteredNames = names.Where(n => n.Length >= 6)
                                                    .OrderBy(n => n.Length).Select(n => n.Length).Count();

            var filtered = names.Where(n => n.Length > 6);
            var ordered = filtered.OrderBy(n => n.Length);
            var projected = ordered.Select(n => n.Length);

            int filteredNamesQuery = (from n in names
                                        where n.Length >= 6
                                        orderby n.Length
                                        select n.ToUpper()).Count();

            foreach (var item in projected)
            {
                Console.WriteLine(item);
            }

            int[] numbers = { 2, 3, 7, 1, 0 };

            var firstTwo = numbers.Take(2);
            var lastThree = numbers.Skip(2);
            var reversed = numbers.Reverse();

            foreach (var item in reversed)
            {
                Console.Write(item + " ");
            }

            Console.ReadKey();
        }
    }
}
