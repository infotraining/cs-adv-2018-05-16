﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace LINQ_Exercise
{
    class Program
    {
        static void Main(string[] args)
        {
            FileInfo[] files;

            using (Stream stream = new FileStream("files.dat", FileMode.Open))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                files = (FileInfo[])formatter.Deserialize(stream);
            }

            //wszystkie plików z rozszerzeniem dll - 2277

            //czy są pliki z 2011 roku

            //najstarszy plik z rozszerzeniem exe

            //pogrupowanie wg rozszerzenia i zwrócenie informacji ile jest rozszerzeń

            //grupowanie po pierwszej literze w nazwie pliku i stworzenie słownika z 5 
            //najczęściej występującymi pierwszymi literami

            var query1 = files.Where(f => string.Equals(f.Extension, ".dll", 
                StringComparison.InvariantCultureIgnoreCase)).Count();

            Console.WriteLine(query1);

            var query2 = files.Where(f => f.FullName.Contains(".dll."));

            foreach (var item in query2)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine(files.Any(f => f.CreationTime.Year == 2011));

            var oldestExe = files.Where(f => string.Equals(f.Extension, ".exe",
                StringComparison.InvariantCultureIgnoreCase))
                .Where(f => f.CreationTime == files.Select(f2 => f2.CreationTime).Min())
                .First();

            Console.WriteLine(oldestExe.CreationTime);

            var stats = from f in files
                        group f by f.Extension.ToLower()
                        into grouping
                        orderby grouping.Key
                        select new { Extension = grouping.Key, Count = grouping.Count() };

            Console.WriteLine(stats.Count());

            foreach (var item in stats)
            {
                Console.WriteLine(item.Extension + ": " + item.Count);
            }

            var result = (from f in files
                          group f by f.Name.ToLower()[0]
                         into grouping
                          orderby grouping.Count() descending
                          select grouping).Take(5).ToDictionary(g => g.Key, g => g.Count());

            foreach (var item in result)
            {
                Console.WriteLine(item.Key + " - " + item.Value);
            }

        }
    }
}
