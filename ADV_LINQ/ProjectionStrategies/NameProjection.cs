﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectionStrategies
{
    class NameProjection
    {
        public string Original { get; set; }
        public string WithNoVowels { get; set; }
    }
}
