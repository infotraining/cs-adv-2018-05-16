﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ProjectionStrategies
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] names = { "Łukasz Zawadzki", "Krzysztof Kowalski", "Rafał Wieleba" };

            //var query = names.Select(n => new NameProjection
            //{
            //    Original = n,
            //    WithNoVowels = Regex.Replace(n, "[aeiouy]", "")
            //});

            //var anonType = from n in names
            //               select new
            //               {
            //                   Original = n,
            //                   WithNoVowels = Regex.Replace(n, "[aeiouy]", "")
            //               };

            var querySyntax = from n in names
                              let noVowels = Regex.Replace(n, "[aeiouy]", "")
                              where noVowels.Length > 9
                              orderby noVowels
                              select n + ": " + noVowels;

            foreach (var item in querySyntax)
            {
                //Console.WriteLine(item.Original + ": " + item.WithNoVowels);
                Console.WriteLine(item);
            }

            IQueryable<string> namesQueryable = names.AsQueryable().Where(n => n.Select(n2 => n2 == 'K').First());

            foreach (var item in namesQueryable)
            {
                Console.WriteLine(item);
            }
        }
    }
}
