﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Linq;

namespace LinqToXmlDemo
{
    class Program
    {
        //static void Main(string[] args)
        //{
        //    XDocument infotraining = XDocument.Load("http://www.infotraining.pl/sitemap.xml");

        //    //foreach (XElement node in infotraining.Elements())
        //    //{
        //    //    foreach (var item in node.Elements())
        //    //    {
        //    //        Console.WriteLine(item);
        //    //    }
        //    //}

        //    XElement element = XElement.Parse(
        //        @"<config>
        //            <client enabled='true'>
        //                <id>30</id>
        //            </client>
        //        </config>");

        //    XElement client = element.Element("client");

        //    Console.WriteLine((bool)client.Attribute("enabled"));

        //    client.Attribute("enabled").SetValue(false);

        //    client.Element("id").SetValue(24);

        //    client.Add(new XElement("name", "Marcin"));

        //    foreach (var item in client.Elements())
        //    {
        //        Console.WriteLine(item);
        //    }

        //    Console.ReadKey();
        //}

        static void Main(string[] args)
        {
            CreateXmlDocument();
            CreateXmlDocumentFromCollection();
            ModifyXml();
        }

        static void ModifyXml()
        {
            XElement fromFile = XElement.Load(@"other_cars.xml");

            foreach (var item in fromFile.Nodes())
            {
                Console.WriteLine(item.ToString(SaveOptions.DisableFormatting));
            }

            IEnumerable<XElement> query = from car in fromFile.Elements()
                                            where car.Elements().Any(c => c.Value.Contains("Fiat"))
                                            select car;

            Console.WriteLine(query.First().Value);

            Console.WriteLine(fromFile.Elements("car").Count());
            
        
            foreach (var child in fromFile.DescendantNodes())
            {
                Console.WriteLine(child.Parent == fromFile);
                Console.WriteLine(child);
            }
        }

        static void CreateXmlDocumentFromCollection()
        {
            Car[] cars =
            {
                new Car { Id = 1, Brand = "Honda", Model = "Civic", Year = 2013},
                new Car { Id = 2, Brand = "Audi", Model = "A6", Year = 1998},
                new Car { Id = 3, Brand = "Fiat", Model = "Punto", Year = 2002},
                new Car { Id = 4, Brand = "Mercedes", Model = "Sprinter", Year = 2005}
            };

            IEnumerable<XElement> carsXml = from car in cars
                                            select new XElement("car",
                                                        new XAttribute("id", car.Id),
                                                        new XElement("brand", car.Brand),
                                                        new XElement("model", car.Model),
                                                        new XElement("year", car.Year)
                                                        );

            XElement root = new XElement("cars");

            root.Add(carsXml);

            root.Save("other_cars.xml");

        }

        static void CreateXmlDocument()
        {
            XDocument doc = new XDocument(new XComment("Lista samochodów w XML"),
                            new XElement("cars",
                                new XElement("car", new XAttribute("id", 1),
                                    new XElement("brand", "Honda"),
                                    new XElement("model", "Civic"),
                                    new XElement("year", 2012)
                                    ),
                                new XElement("car", new XAttribute("id", 1),
                                    new XElement("brand", "Mazda"),
                                    new XElement("model", "RX6"),
                                    new XElement("year", 2005)
                                    )
                                )
                            );

            doc.Save(@"cars.xml");
        }
    }

    class Car
    {
        public int Id { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
    }
}
