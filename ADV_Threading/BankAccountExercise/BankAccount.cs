﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankAccountExercise
{
    //niedeterministyczne
    //class BankAccount
    //{
    //    private decimal _balance;

    //    public BankAccount(int id, decimal balance)
    //    {
    //        Id = id;
    //        _balance = balance;
    //    }

    //    public int Id { get; private set; }

    //    public decimal Balance
    //    {
    //        get { return _balance; }
    //        private set { _balance = value; }
    //    }

    //    public void Deposit(decimal amount)
    //    {
    //        Balance += amount;
    //    }

    //    public void Withdraw(decimal amount)
    //    {
    //        Balance -= amount;
    //    }

    //    public void Tranfer(BankAccount transferTo, decimal amount)
    //    {
    //        Withdraw(amount);
    //        transferTo.Deposit(amount);
    //    }
    //}

    class BankAccount
    {
        private decimal _balance;
        private object _locker = new object();

        public BankAccount(int id, decimal balance)
        {
            Id = id;
            _balance = balance;
        }

        public int Id { get; private set; }

        public decimal Balance
        {
            get
            {
                lock (_locker)
                {
                    return _balance;
                }
            }
            private set { _balance = value; }
        }

        public void Deposit(decimal amount)
        {
            lock (_locker)
            {
                Balance += amount;
            }
        }

        public void Withdraw(decimal amount)
        {
            lock (_locker)
            {
                Balance -= amount;
            }
        }

        public void Tranfer(BankAccount transferTo, decimal amount)
        {
            //Withdraw(amount);

            //transferTo.Deposit(amount);

            object firstLock, secondLock;

            if (this.Id < transferTo.Id)
            {
                firstLock = this._locker;
                secondLock = transferTo._locker;
            }
            else
            {
                firstLock = transferTo._locker;
                secondLock = this._locker;
            }

            lock (firstLock)
            {
                _balance -= amount;
                lock (secondLock)
                {
                    transferTo._balance += amount;
                }
            }
        }
    }
}
