﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankAccountExercise
{
    class Program
    {
        static void Main(string[] args)
        {
            var ba1 = new BankAccount(1, 10000.0M);
            var ba2 = new BankAccount(2, 10000.0M);
            var ba3 = new BankAccount(3, 10000.0M);

            Console.WriteLine($"ba1.Balance = {ba1.Balance}, ba2.Balance = {ba2.Balance}, ba3.Balance = {ba3.Balance}");

            var transfers1 = Task.Run(() =>
            {
                for (int i = 0; i < 10000; i++)
                {
                    ba1.Tranfer(ba2, 1.0M);
                }
            });

            var transfers2 = Task.Run(() =>
            {
                for (int i = 0; i < 10000; i++)
                {
                    ba2.Tranfer(ba1, 1.0M);
                }
            });

            var transfers3 = Task.Run(() =>
            {
                for (int i = 0; i < 10000; i++)
                {
                    ba2.Tranfer(ba3, 1.0M);
                }
            });

            var transfers4 = Task.Run(() =>
            {
                for (int i = 0; i < 10000; i++)
                {
                    ba3.Tranfer(ba2, 1.0M);
                }
            });

            try
            {
                Task.WaitAll(transfers1, transfers2, transfers3, transfers4);
            }
            catch (AggregateException ae)
            {
                ae.Handle(e => { Console.WriteLine(e.Message); return false; });
            }

            Console.WriteLine($"ba1.Balance = {ba1.Balance}, ba2.Balance = {ba2.Balance}, ba3.Balance = {ba3.Balance}");
        }
    }
}
