﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task task = Task.Run(() => DisplayPrimes());

            //for (int i = 0; i < 10; i++)
            //{
            //    Thread.Sleep(1000);

            //    Console.WriteLine(i);
            //}

            WhenDone().Wait();
        }

        static async Task WhenDone()
        {
            await PrintSum();
            Console.WriteLine("Gotowe!");
        }

        static async Task PrintSum()
        {
            int sum = await GetSum(4, 5);
            Console.WriteLine(sum);
        }

        static async Task<int> GetSum(int a, int b)
        {
            await Task.Delay(2500);

            return a + b;
        }

        static int GetPrimes(int start, int count)
        {
            return ParallelEnumerable.Range(start, count)
                .Count(n => Enumerable.Range(2, (int)Math.Sqrt(n)).All(i => n % i > 0));
        }

        static Task<int> GetPrimesAsync(int start, int count)
        {
            return Task.Run(() => ParallelEnumerable.Range(start, count)
                .Count(n => Enumerable.Range(2, (int)Math.Sqrt(n)).All(i => n % i > 0)));
        }

        static void DisplayPrimes()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(GetPrimes(i * 1000000 + 2, 1000000)
                    + " jest liczb pierwszych w zakresie " 
                    + (i * 1000000 + 2).ToString() + " - " + ((i + 1) * 1000000));
            }

            Console.WriteLine("Gotowe!");
        }

        static async void DisplayPrimesAsync()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(await GetPrimesAsync(i * 1000000 + 2, 1000000)
                    + " jest liczb pierwszych w zakresie "
                    + (i * 1000000 + 2).ToString() + " - " + ((i + 1) * 1000000));
            }

            Console.WriteLine("Gotowe!");
        }
    }
}
