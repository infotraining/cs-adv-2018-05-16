﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsThreading
{
    public partial class Form1 : Form
    {
        SynchronizationContext _syncContext;

        public Form1()
        {
            InitializeComponent();
            _syncContext = SynchronizationContext.Current;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Thread worker = new Thread(() => {
            //            DoExtensiveWork();
            //            UpdateListBox("Gotowe!");
            //});

            //worker.Start();

            Task task = Task.Run(() => DoExtensiveWork());

            var awaiter = task.GetAwaiter();

            awaiter.OnCompleted(() => {
                listBox1.Items.Add("Gotowe!");
            });
        }

        private void UpdateListBox(string message)
        {
            //Action action = () => listBox1.Items.Add(message);
            //this.BeginInvoke(action);
            _syncContext.Post(_ => listBox1.Items.Add(message), null);
        }

        private void DoExtensiveWork()
        {
            Thread.Sleep(10);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 10; i++)
            {
                listBox1.Items.Add("Iteracja: " + i);
            }
        }
    }
}
