﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncCancellation
{
    class Program
    {
        class CancellationToken
        {
            public bool IsCancellationRequested { get; private set; }
            public void Cancel()
            {
                IsCancellationRequested = true;
            }
            public void ThrowIfCancelled()
            {
                if (IsCancellationRequested)
                    throw new OperationCanceledException();
            }
        }

        static void Main(string[] args)
        {
            var token = new CancellationToken();

            Execute(token);

            Thread.Sleep(5000);

            token.Cancel();
        }

        static async Task Execute(CancellationToken token)
        {
            for (int i = 0; i < 100; i++)
            {
                Console.WriteLine(i);
                await Task.Delay(1000);
                token.ThrowIfCancelled();
            }
        }
    }
}
