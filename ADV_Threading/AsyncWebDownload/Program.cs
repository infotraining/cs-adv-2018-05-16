﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace AsyncWebDownload
{
    class Program
    {
        static void Main(string[] args)
        {
            Download();

            Console.ReadKey();
        }

        static async void Download()
        {
            string[] urls = "infotraining.pl onet.pl interia.pl".Split();

            int totalLength = 0;

            try
            {
                foreach (var url in urls)
                {
                    var uri = new Uri("http://" + url);

                    var data = await new WebClient().DownloadDataTaskAsync(uri);

                    Console.WriteLine("Długość urla " + url + " to " + data.Length);

                    totalLength += data.Length;
                }
                Console.WriteLine("Całkowita długość: " + totalLength);
            }
            catch (WebException ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
    }
}
