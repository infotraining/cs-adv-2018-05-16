﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Signalization
{
    class Program
    {
        //static EventWaitHandle wh = new AutoResetEvent(false);
        static EventWaitHandle wh = new ManualResetEvent(false);
        static CountdownEvent ce = new CountdownEvent(3);

        static void WorkerThread(int i)
        {
            Console.WriteLine("Worker is ready for job");
            wh.WaitOne();
            Console.WriteLine("Job has started...");
            Thread.Sleep(2500*i);
            Console.WriteLine("Job done!");
            ce.Signal();
        }

        static void Main(string[] args)
        {
            Thread thread1 = new Thread(() => WorkerThread(1));
            thread1.Start();
            Thread thread2 = new Thread(() => WorkerThread(2));
            thread2.Start();
            Thread thread3 = new Thread(() => WorkerThread(3));
            thread3.Start();

            Console.WriteLine("Press ENTER to let worker start his job...");
            Console.ReadLine();

            wh.Set();

            ce.Wait();

            Console.WriteLine("All threads are done");
        }
    }
}
