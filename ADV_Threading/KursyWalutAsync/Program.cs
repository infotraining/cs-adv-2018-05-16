﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace KursyWalutAsync
{
    class Program
    {
        //static void Main(string[] args)
        //{
        //    WebClient client = new WebClient();

        //    string kursy = client.DownloadString(@"http://rss.nbp.pl/kursy/TabelaA.xml");

        //    var xmlKursy = XElement.Parse(kursy);

        //    var tabele = xmlKursy.Descendants("item")
        //                            .Select(item => new
        //                            {
        //                                Title = item.Element("title").Value,
        //                                Url = item.Element("enclosure").Attribute("url").Value
        //                            });

        //    foreach (var tabela in tabele)
        //    {
        //        Console.WriteLine(tabela.Title);

        //        string[] waluty = { "USD", "GBP", "EUR" };

        //        string tabelaKursow = client.DownloadString(tabela.Url);

        //        var tabelaXml = XElement.Parse(tabelaKursow);

        //        var kursyWalut = from item in tabelaXml.Elements("pozycja")
        //                         where waluty.Contains(item.Element("kod_waluty").Value)
        //                         select new
        //                         {
        //                             Kod = item.Element("kod_waluty").Value,
        //                             Przelicznik = item.Element("kurs_sredni").Value
        //                         };

        //        foreach (var kurs in kursyWalut)
        //        {
        //            Console.WriteLine(kurs.Kod + ": " + kurs.Przelicznik);
        //        }
        //    }
        //}

        static void Main(string[] args)
        {
            ProcessAsync().Wait();
        }

        static async Task ProcessAsync()
        {
            WebClient client = new WebClient();

            string kursy = client.DownloadString(@"http://rss.nbp.pl/kursy/TabelaA.xml");

            var xmlKursy = XElement.Parse(kursy);

            var tabele = xmlKursy.Descendants("item")
                                    .Select(item => new
                                    {
                                        Title = item.Element("title").Value,
                                        Url = item.Element("enclosure").Attribute("url").Value
                                    });

            List<Task<string>> tasks = new List<Task<string>>();

            foreach (var tabela in tabele)
            {
                tasks.Add(Task.Run(() => new WebClient().DownloadString(tabela.Url)));
            }
            
            string[] daneNBP = await Task.WhenAll(tasks);

            foreach (var tabela in tabele)
            {
                Console.WriteLine(tabela.Title);

                string[] waluty = { "USD", "GBP", "EUR" };

                string tabelaKursow = client.DownloadString(tabela.Url);

                var tabelaXml = XElement.Parse(tabelaKursow);

                var kursyWalut = from item in tabelaXml.Elements("pozycja")
                                 where waluty.Contains(item.Element("kod_waluty").Value)
                                 select new
                                 {
                                     Kod = item.Element("kod_waluty").Value,
                                     Przelicznik = item.Element("kurs_sredni").Value
                                 };

                foreach (var kurs in kursyWalut)
                {
                    Console.WriteLine(kurs.Kod + ": " + kurs.Przelicznik);
                }
            }
        }
    }
}
