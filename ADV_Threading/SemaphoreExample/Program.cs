﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SemaphoreExample
{
    class Program
    {
        static Semaphore _semaphore = new Semaphore(3, 3);

        static void Main(string[] args)
        {
            for (int i = 1; i < 11; i++)
            {
                new Thread(Enter).Start(i);
            }
        }

        static void Enter(object id)
        {
            Console.WriteLine($"Wątek {id} chce wejść...");
            _semaphore.WaitOne();
            Console.WriteLine($"Wątek {id} jest w środku!");
            Thread.Sleep(1000 * (int)id);
            _semaphore.Release();
            Console.WriteLine($"Wątek {id} wyszedł!");
        }
    }
}
