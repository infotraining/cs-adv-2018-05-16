﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PiExercise
{
    class Program
    {
        const long N = 300000000;

        static void Main(string[] args)
        {
            //SingleThreadPi();
            //ThreadUnsafePi();
            ThreadSafePi();
            TaskPi();
        }

        static void TaskPi()
        {
            Console.WriteLine("Tasks Pi started...");
            Stopwatch sw = new Stopwatch();

            int numberOfThread = Environment.ProcessorCount;

            sw.Start();

            long counter = 0;

            List<Task<long>> tasks = new List<Task<long>>();

            long hitsPerThread = N / numberOfThread;
            long[] countPerThread = new long[numberOfThread];

            ThreadLocal<Random> random =
                new ThreadLocal<Random>(() => new Random(Guid.NewGuid().GetHashCode()));


            for (long i = 0; i < numberOfThread; i++)
            {
                long index = i;

                tasks.Add(Task<long>.Factory.StartNew(() =>
                {
                    long hits = 0;

                    var randomGen = random.Value;

                    for (int j = 0; j < hitsPerThread; j++)
                    {
                        double x = randomGen.NextDouble() * 2 - 1.0;
                        double y = randomGen.NextDouble() * 2 - 1.0;

                        double length = Math.Sqrt(x * x + y * y);

                        if (length < 1.0)
                            ++hits;
                    }

                    return hits;
                }, TaskCreationOptions.LongRunning));
            }

            counter = tasks.Aggregate(0L, (i, t) => i + t.Result);

            double pi = ((double)counter / N) * 4;

            sw.Stop();

            Console.WriteLine($"PI to {pi}");
            Console.WriteLine($"Policzone w {sw.Elapsed}");
        }

        static void ThreadSafePi()
        {
            Console.WriteLine("Thread safe Pi started...");
            Stopwatch sw = new Stopwatch();

            int numberOfThread = Environment.ProcessorCount;

            sw.Start();

            long counter = 0;

            List<Thread> threads = new List<Thread>();

            long hitsPerThread = N / numberOfThread;
            long[] countPerThread = new long[numberOfThread];

            ThreadLocal<Random> random = 
                new ThreadLocal<Random>(() => new Random(Guid.NewGuid().GetHashCode()));

            //Stopwatch sw1 = new Stopwatch();
            //sw1.Start();
            //double x1 = random.Value.NextDouble() * 2 - 1.0;
            //sw1.Stop();
            //Console.WriteLine(sw1.Elapsed.TotalMilliseconds * N);
        


            for (long i = 0; i < numberOfThread; i++)
            {
                long index = i;

                var thread = new Thread(() =>
                {
                    long hits = 0;

                    var randomGen = random.Value;

                    for (int j = 0; j < hitsPerThread; j++)
                    {
                        double x = randomGen.NextDouble() * 2 - 1.0;
                        double y = randomGen.NextDouble() * 2 - 1.0;

                        double length = Math.Sqrt(x * x + y * y);

                        if (length < 1.0)
                            ++hits;
                    }

                    countPerThread[index] = hits;
                });

                thread.Start();
                threads.Add(thread);
            }

            foreach (var thread in threads)
            {
                thread.Join();
            }

            counter = countPerThread.Sum();

            double pi = ((double)counter / N) * 4;

            sw.Stop();

            Console.WriteLine($"PI to {pi}");
            Console.WriteLine($"Policzone w {sw.Elapsed}");
        }

        static void ThreadUnsafePi()
        {
            Console.WriteLine("Single thread Pi started...");
            Stopwatch sw = new Stopwatch();

            int numberOfThread = Environment.ProcessorCount;

            sw.Start();

            long counter = 0;

            List<Thread> threads = new List<Thread>();

            long hitsPerThread = N / numberOfThread;

            Random random = new Random(Guid.NewGuid().GetHashCode());

            for (long i = 0; i < numberOfThread; i++)
            {
                var thread = new Thread(() =>
                {
                    for (int j = 0; j < hitsPerThread; j++)
                    {
                        double x = random.NextDouble() * 2 - 1.0;
                        double y = random.NextDouble() * 2 - 1.0;

                        double length = Math.Sqrt(x * x + y * y);

                        if (length < 1.0)
                            ++counter;
                    }
                });

                thread.Start();
                threads.Add(thread);
            }

            foreach (var thread in threads)
            {
                thread.Join();
            }

            double pi = ((double)counter / N) * 4;

            sw.Stop();

            Console.WriteLine($"PI to {pi}");
            Console.WriteLine($"Policzone w {sw.Elapsed}");
        }

        static void SingleThreadPi()
        {
            Console.WriteLine("Single thread Pi started...");
            Stopwatch sw = new Stopwatch();

            sw.Start();

            long counter = 0;
            Random random = new Random(Guid.NewGuid().GetHashCode());

            for (long i = 0; i < N; i++)
            {
                double x = random.NextDouble() * 2 - 1.0;
                double y = random.NextDouble() * 2 - 1.0;

                double length = Math.Sqrt(x * x + y * y);

                if (length < 1.0)
                    ++counter;
            }

            double pi = ((double)counter / N) * 4;

            sw.Stop();

            Console.WriteLine($"PI to {pi}");
            Console.WriteLine($"Policzone w {sw.Elapsed}");
        }
    }
}
