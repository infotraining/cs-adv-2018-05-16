﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadsBasics
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread t = new Thread(Write);
            t.Priority = ThreadPriority.Lowest;
            t.Start();
            Thread t1 = new Thread(Write);
            t1.Priority = ThreadPriority.Highest;
            t1.Start();
            //for (int i = 0; i < 1000; i++)
            //{
            //    Console.WriteLine("Y");
            //}

            Console.WriteLine("Done!");
        }

        static void Write()
        {
            for (int i = 0; i < 100; i++)
            {
                Console.WriteLine("X" + Thread.CurrentThread.ManagedThreadId);
            }
        }
    }
}
