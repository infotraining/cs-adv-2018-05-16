﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ExclusiveThreadLocking
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] values = { 0, 20, 34, 2, 5, 34, 5, 4 };
            int[] dividers = { 0, 3, 65, 0, 0, 2, 0, 4 };

            for (int i = 0; i < 100; i++)
            {
                for (int j = 0; j < values.Length; j++)
                {
                    int temp = j;

                    ThreadUnsafe.Set(values[temp], dividers[temp]);

                    new Thread(() => ThreadUnsafe.Divide()).Start();
                }
            }
        }
    }

    //teraz już Safe!
    class ThreadUnsafe
    {
        static int _val1 = 1, _val2 = 1;
        static readonly object _locker = new Object();

        public static void Set(int a, int b)
        {
            lock (_locker)
            {
                _val1 = a;
                _val2 = b;
            }
        }

        public static void Divide()
        {
            lock (_locker)
            {
                if (_val2 != 0)
                {
                    Thread.Sleep(10);
                    Console.WriteLine(_val1 + "/" + _val2 + " = " + _val1 / _val2);
                }
            }
        }
    }
}
