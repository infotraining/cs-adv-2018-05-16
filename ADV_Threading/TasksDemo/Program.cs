﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TasksDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var task = Task.Run(() => Console.WriteLine("Komunikat z Task"));

            var task2 = Task.Factory.StartNew(
                () => Console.WriteLine("Task stworzony z Task.Factory"), TaskCreationOptions.LongRunning);

            Task<int> task3 = Task.Run(() => Add(4, 6));

            task.Wait();

            Console.WriteLine(task3.Result);
            Task task4 = Task.Run(() => Print());
            Task task5 = Task.Run(() => Print());

            try
            {
                task4.Wait();
                task5.Wait();
            }
            catch (AggregateException aex)
            {
                Console.WriteLine(aex.InnerExceptions.First().Message);
            }

            Task<int> task6 = Task.Run(() => Print("Wiadomość do drukowania"));

            var awaiter = task6.GetAwaiter();

            
            
            awaiter.OnCompleted(() =>
            {
                int result = awaiter.GetResult();
                Console.WriteLine("Wiadomość miała " + result + " znaków");
            });

            Task task7 = task6.ContinueWith(a => {
                Console.WriteLine("Metoda kontynuacji...");
            });

            Console.ReadKey();
        }

        private static void Print()
        {
            throw new NotImplementedException();
        }

        private static int Print(string message)
        {
            Thread.Sleep(1000);
            Console.WriteLine(message);
            return message.Length;
        }

        private static int Add(int v1, int v2)
        {
            return v1 + v2;
        }
    }
}
