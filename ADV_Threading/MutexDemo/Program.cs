﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MutexDemo
{
    class Program
    {
        static Mutex mutex = new Mutex(false, "MultiprocessMutex.Exe");

        static void Main(string[] args)
        {
            if (!mutex.WaitOne(TimeSpan.FromSeconds(5)))
            {
                Console.WriteLine("Program jest już wykorzystywany. Do widzenia!");
                Thread.Sleep(1000);
                return;
            }

            try
            {
                Console.WriteLine("Uruchomiono MutexDemo. Naciśnij Enter...");
                Console.ReadLine();
            }
            finally
            {
                mutex.ReleaseMutex();
            }
        }
    }
}
