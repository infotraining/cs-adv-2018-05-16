﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadLocalStorage
{
    class Program
    {
        static void Main(string[] args)
        {
            //var localRandom = new ThreadLocal<Random>(() => new Random());
            var localRandom = new ThreadLocal<int>(() => Guid.NewGuid().GetHashCode());

            Thread t1 = new Thread(() => Console.WriteLine(localRandom.Value));
            Thread t2 = new Thread(() => Console.WriteLine(localRandom.Value));
            
            t1.Start();
            t2.Start();
        }
    }
}
