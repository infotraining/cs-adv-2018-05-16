﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskCombinators
{
    class Program
    {
        static void Main(string[] args)
        {
            WhenAny();

            WhenAll();

            Console.ReadKey();
        }

        static async void WhenAny()
        {
            async Task<int> Delay1() { await Task.Delay(1000); return 1; }
            async Task<int> Delay2() { await Task.Delay(2000); return 2; }
            async Task<int> Delay3() { await Task.Delay(3000); return 3; }

            Task<int> winTask = await Task.WhenAny(Delay1(), Delay2(), Delay3());

            Console.WriteLine(winTask.Result);
            Console.WriteLine("Gotowe!");
        }

        static async void WhenAll()
        {
            async Task<int> Delay1() { await Task.Delay(1000); return 1; }
            async Task<int> Delay2() { await Task.Delay(2000); return 2; }
            async Task<int> Delay3() { await Task.Delay(3000); return 3; }

            int[] results = await Task.WhenAll(Delay1(), Delay2(), Delay3());

            foreach (var result in results)
            {
                Console.WriteLine(result);
            }
            Console.WriteLine("Gotowe!");
        }
    }
}
