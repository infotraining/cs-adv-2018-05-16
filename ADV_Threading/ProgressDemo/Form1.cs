﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProgressDemo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private async Task<double> Calculate(int number)
        {
            await Task.Delay(100);

            double sqrt = Math.Sqrt(number);

            return sqrt;
        }

        public async void DoWork(IProgress<int> progress)
        {
            for (int i = 1; i < 101; i++)
            {
                if (progress != null)
                {
                    progress.Report(i);
                }

                listBox1.Items.Add(await Calculate(i));
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            progressBar1.Maximum = 100;
            progressBar1.Step = 1;

            var progress = new Progress<int>(v => progressBar1.Value = v);
                        
            DoWork(progress);
        }
    }
}
