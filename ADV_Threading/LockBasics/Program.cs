﻿using System;
using System.Threading;

namespace LockBasics
{
    class Program
    {
        static bool _done;
        static readonly object _locker = new Object();

        static void Main(string[] args)
        {
            //for (int i = 0; i < 10; i++)
            //{
            //    new Thread(Execute).Start();
            //}

            //Thread t = new Thread(() => Print("Wywołane z wątku"));

            //t.Start();

            ////zmienne zewnętrzne
            //for (int i = 0; i < 10; i++)
            //{
            //    string temp = i.ToString();
            //    new Thread(() => Print(temp)).Start();
            //}

            //new Thread(FaultyMethod).Start();

            Thread t = new Thread(() => Print("Wywołane z wątku"));

            t.IsBackground = true;

            t.Start();

            Console.WriteLine("Główna metoda się skończyła");

            t.Join();
        }

        static void FaultyMethod()
        {
            try
            {
                throw new ArgumentOutOfRangeException();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        static void Print(string message)
        {
            Thread.Sleep(1000);
            Console.WriteLine(message);
        }

        static void Execute()
        {
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine(i + ": " + Thread.CurrentThread.ManagedThreadId);
            }

            lock (_locker)
            {
                if (!_done)
                {
                    Thread.Sleep(1000);
                    _done = true;
                    Console.WriteLine("Program się wykonał!");
                }
            }
        }
    }
}
