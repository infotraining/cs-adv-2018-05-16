﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParallelPrimes
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            const int N = 2000000000;

            Console.WriteLine("SyncPrimes");

            var primes = Enumerable.Range(2, N)
                        .Where(n => Enumerable.Range(2, (int)Math.Sqrt(n)).All(i => n % i > 0));
            

            sw.Stop();

            Console.WriteLine($"Wyliczono w {sw.Elapsed}");

            sw.Reset();
            sw.Start();

            Console.WriteLine("ParallelPrimes");

            var primes1 = Enumerable.Range(2, N).AsParallel()
                        .Where(n => Enumerable.Range(2, (int)Math.Sqrt(n)).All(i => n % i > 0));
            

            sw.Stop();

            Console.WriteLine($"Wyliczono w {sw.Elapsed}");
        }
    }
}
