﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParallelFor
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(i);
            }
            
            Parallel.For(0, 10, i => { Console.WriteLine(i);});

            Parallel.For(0, 10, Print);

            //Parallel.ForEach("Szkolenie z C#", Print);

            foreach (char c in "Szkolenie z C#")
            {
                Print(c);
            }
        }

        static void Print(int i)
        {
            Console.WriteLine("Message " + i);
        }
    }
}
