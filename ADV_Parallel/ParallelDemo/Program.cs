﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParallelDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var text = "abcdef".AsParallel().Select(c => char.ToUpper(c)).ToArray();

            Console.WriteLine(text);

            var text2 = "abcdef".AsParallel().AsOrdered().Select(c => char.ToUpper(c)).ToArray();

            Console.WriteLine(text2);
        }
    }
}
