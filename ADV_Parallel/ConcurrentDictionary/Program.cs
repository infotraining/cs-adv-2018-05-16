﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConcurrentDictionary
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch w = new Stopwatch();
            w.Start();
            var d = new ConcurrentDictionary<int, int>();
            for (int i = 0; i < 100000; i++)
            {
                d[i] = i;
            }
            w.Stop();
            Console.WriteLine($"Elapsed {w.Elapsed}");
            w.Restart();
            var d1 = new Dictionary<int, int>();
            for (int i = 0; i < 100000; i++)
            {
                lock (d1)
                {
                    d1[i] = i;
                }
            }
            w.Stop();
            Console.WriteLine($"Elapsed {w.Elapsed}");
            
        }
    }
}
