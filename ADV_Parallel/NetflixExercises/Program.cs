﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetflixExercises
{
    class Program
    {
        static void Main(string[] args)
        {
            Sequential();
            PLINQ();
            ConcurrentDictionary();
        }

        static void ConcurrentDictionary()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            Stopwatch sw1 = new Stopwatch();
            sw1.Start();

            string infile = "ratings.txt";

            Splash(infile);

            var file = File.ReadAllLines(infile);

            sw1.Stop();

            ConcurrentDictionary<int, int> reviewsByUser = new ConcurrentDictionary<int, int>();

            Console.WriteLine($"Czytanie pliku zajęło {sw1.Elapsed}");

            //var reviewsByUser = file.AsParallel().Select(line => Parse(line))
            //                           .GroupBy(id => { return id; })
            //                           .Select(g => new { UserId = g.Key, NumReviews = g.Count() });

            Parallel.ForEach(file,
                new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount },
                (line) =>
                {
                    int userId = Parse(line);

                    reviewsByUser.AddOrUpdate(userId, 1, (uid, count) => { return ++count; });
                });

            var sorted = from user in reviewsByUser.AsParallel()
                         orderby user.Value descending, user.Key ascending
                         select new { UserId = user.Key, NumReviews = user.Value };

            sw.Stop();

            var top10 = sorted.Take(10).ToList();

            Console.WriteLine();
            Console.WriteLine("Top 10 użytkowników: ");

            foreach (var top in top10)
            {
                Console.WriteLine($"{top.UserId}: {top.NumReviews}");
            }

            Console.WriteLine();
            Console.WriteLine($"Obliczone w {sw.Elapsed}");
            Console.WriteLine();
        }

        static void PLINQ()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            Stopwatch sw1 = new Stopwatch();
            sw1.Start();

            string infile = "ratings.txt";

            Splash(infile);

            var file = File.ReadAllLines(infile);

            sw1.Stop();

            Console.WriteLine($"Czytanie pliku zajęło {sw1.Elapsed}");

            var reviewsByUser = file.AsParallel().Select(line => Parse(line))
                                       .GroupBy(id => { return id; })
                                       .Select(g => new { UserId = g.Key, NumReviews = g.Count() });

            var sorted = from user in reviewsByUser.AsParallel()
                         orderby user.NumReviews descending, user.UserId ascending
                         select user;

            sw.Stop();

            var top10 = sorted.Take(10).ToList();

            Console.WriteLine();
            Console.WriteLine("Top 10 użytkowników: ");

            foreach (var top in top10)
            {
                Console.WriteLine($"{top.UserId}: {top.NumReviews}");
            }

            Console.WriteLine();
            Console.WriteLine($"Obliczone w {sw.Elapsed}");
            Console.WriteLine();

        }

        static void Sequential()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            string infile = "ratings.txt";

            Splash(infile);

            Dictionary<int, int> reviewsByUser = new Dictionary<int, int>();

            var file = File.ReadAllLines(infile);

            foreach (var line in file)
            {
                int userId = Parse(line);

                if (!reviewsByUser.ContainsKey(userId))
                {
                    reviewsByUser.Add(userId, 1);
                }
                else
                {
                    reviewsByUser[userId]++;
                }
            }

            var sorted = from user in reviewsByUser
                         orderby user.Value descending, user.Key ascending
                         select new { UserId = user.Key, NumReviews = user.Value };

            sw.Stop();

            var top10 = sorted.Take(10).ToList();

            Console.WriteLine();
            Console.WriteLine("Top 10 użytkowników: ");

            foreach (var top in top10)
            {
                Console.WriteLine($"{top.UserId}: {top.NumReviews}");
            }

            Console.WriteLine();
            Console.WriteLine($"Obliczone w {sw.Elapsed}");
            Console.WriteLine();
        }

        static int Parse(string line)
        {
            char[] separators = { ',' };
            string[] tokens = line.Split(separators);

            return Convert.ToInt32(tokens[1]);
        }

        static void Splash(string infile)
        {
            if (!File.Exists(infile))
            {
                Console.WriteLine("Plik nie istnieje");
                System.Environment.Exit(-1);
            }

            FileInfo fi = new FileInfo(infile);

            double size = fi.Length / 1048576.0;

            Console.WriteLine("Netflix Mining App");
            Console.WriteLine($"  File: {infile}, Size: {size} ");
            Console.WriteLine();
        }
    }
}
